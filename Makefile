packer/output-qemu/dnscache.qcow2: packer/ephemeral/dnscache.tar packer/ephemeral/installercore.iso
	-rm packer/output-qemu/dnscache.qcow2
	cd packer && make output-qemu/dnscache.qcow2

packer/output-qemu/dnscache.qcow2.xz: packer/output-qemu/dnscache.qcow2
	xz -T0 packer/output-qemu/dnscache.qcow2

scratch-interactive: packer/output-qemu/dnscache.qcow2 metadata/mddata.iso
	qemu-system-x86_64 -accel kvm -nographic -m 256 -drive file=packer/output-qemu/dnscache.qcow2,if=virtio \
		-cdrom metadata/mddata.iso \

packer/ephemeral/dnscache.tar: build.sh docker/Dockerfile
	CODEBASE=dnscache ./build.sh
	-docker rm export
	docker run --name export build/release true
	docker export export > packer/ephemeral/dnscache.tar

packer/ephemeral/installercore.iso:
	docker run --rm=true -v $$(pwd)/packer/ephemeral:/workdir:Z registry.gitlab.com/untrustedhost/installenv
	sudo chown $$(whoami) packer/ephemeral/installercore.iso

metadata/mddata.iso: metadata/Makefile metadata/makeiso.sh metadata/mangle-example.sh metadata/virt-install.xml
	cd metadata && make mddata.iso
